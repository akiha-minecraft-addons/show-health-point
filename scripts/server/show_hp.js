/**
 * このスクリプトの設定
 */
const settings = {
  // 名前を常に表示するか
  always_show: true,
  // HP表示を更新する間隔(攻撃時は即時適用される)
  update_ticks: 60, // 3秒
};

/* global server */
const system = server.registerSystem(0, 0);

// 更新するエンティティの配列
let updateEntityList = [];
// 取得するエンティティのクエリを入れる
let customQuery = {};
// ティック数を数える変数
let tick = 0;

/**
 * 元の名前を取得する
 * @param {Component JS API object} nametag
 * @returns {string}
 */
function getOriginalName(nametag) {
  // 名前を持っていなければ空の文字列を返す
  if (!nametag.data.name) {
    return '';
  }
  // 名前を改行コードで分割して、その1つ目を取得する
  const firstLine = nametag.data.name.split('\n')[0];
  // そこにスラッシュが含まれていなければ、元の名前と判定する
  if (!firstLine.includes('/')) {
    return firstLine;
  }
  // それ以外は空の文字列を返す
  return '';
}

/**
 * 新しい名前を取得する
 * @param {Component JS API object} health
 * @param {Component JS API object} nametag
 * @returns {sting}
 */
function getNewName(health, nametag) {
  // 現在のHPの状態を文字列にする
  const hp = `${health.data.value}/${health.data.max}`;
  // 元の名前を取得する
  const originalName = getOriginalName(nametag);
  // 元の名前があれば
  if (originalName) {
    // 元の名前から改行してHPを追加する。
    // "\n"(バックスラッシュ・エヌ)は改行コード
    return `${originalName}\n${hp}`;
  }
  // それ以外は新しいHPだけ
  return hp;
}

// 最初に実行されるメソッド
system.initialize = function initialize() {
  // エンティティがダメージを受けたとき
  this.listenForEvent(
    'minecraft:entity_hurt',
    (eventData) => this.onEntityHurt(eventData),
  );
  // エンティティが生成されたとき
  this.listenForEvent(
    'minecraft:entity_created',
    (eventData) => this.onEntityCreated(eventData),
  );
  // エンティティ取得のクエリ
  // 特になにも設定していないのでエンティティすべてを取得する
  customQuery = this.registerQuery();
};

// チャット欄に文字を表示する
system.displayChat = function displayChat(message) {
  const BroadcastEventData = this.createEventData('minecraft:display_chat_event');
  BroadcastEventData.data.message = message;
  this.broadcastEvent('minecraft:display_chat_event', BroadcastEventData);
};

// 毎ティック実行されるメソッド
system.update = function update() {
  // 更新配列に入っているエンティティのHP表示を更新する
  // 急がなくてもいいので、負荷分散のため1ティック5回に限定する
  for (let i = 0; i < 5; i += 1) {
    // 配列に値が入っているか
    if (updateEntityList.length) {
      // shift()は配列の先頭から値を取り出す
      this.updateName(updateEntityList.shift());
    } else {
      // 配列が空なら繰り返しを終了
      break;
    }
  }

  // HP表示を更新するエンティティを取得する
  // 毎ティックやらなくてもいいので、負荷分散のために間隔をあける
  if (tick % settings.update_ticks === 0) {
    // 存在しているエンティティの配列を取得して更新用の配列と結合させる
    updateEntityList = updateEntityList.concat(this.getEntitiesFromQuery(customQuery));
  }

  // ティック数を数える
  tick += 1;
};

// エンティティがダメージを受けたときに実行される
system.onEntityHurt = function onEntityHurt(eventData) {
  // ダメージを受けたエンティティを更新用の配列に格納
  // この段階ではまだHPが減っていないので、後で表示HPを更新する必要がある
  // unshift()は配列の先頭に入れる -> 優先的に処理
  updateEntityList.unshift(eventData.data.entity);
};

// エンティティが生成されたときに実行される
system.onEntityCreated = function onEntityCreated(eventData) {
  // 更新配列の先頭に入れる
  updateEntityList.unshift(eventData.data.entity);
};

// 名前を更新する
system.updateName = function updateName(entity) {
  // 最初にそのエンティティがHP表示の対象になるかを判定する
  // 判定のための文字列が長いので短い名前の変数に入れている
  const hasHealth = this.hasComponent(entity, 'minecraft:health');
  const hasNameable = this.hasComponent(entity, 'minecraft:nameable');
  // そのエンティティが両方のコンポーネントを持っていなければ処理を中断
  // (Aでない、またはBでない) = ((AかつB)でない)
  // if (!hasHealth || !hasNameable) {
  if (!(hasHealth && hasNameable)) {
    return;
  }

  // コンポーネントを取得
  const health = this.getComponent(entity, 'minecraft:health');
  const nametag = this.getComponent(entity, 'minecraft:nameable');

  // 名前にHPを加えて設定
  nametag.data.name = getNewName(health, nametag);
  // 名前を常に表示する
  if (settings.always_show) {
    nametag.data.always_show = true;
  }
  // 変更点を適用する
  this.applyComponentChanges(entity, nametag);
};
